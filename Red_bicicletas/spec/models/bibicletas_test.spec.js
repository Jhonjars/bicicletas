let mongoose = require('mongoose');
let Bicicleta= require('../../models/bicicleta');


describe('texting bicicletas',()=>{
    beforeEach((done)=>{
        var mongoDB= 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser:true})
  
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'conection error'));
    db.once('open', ()=>{
        console.log("we are connected to test")
        done()
         });
    });

    afterEach((done)=>{
        Bicicleta.deleteMany({},(err,success)=>{
            if (err) console.log(err);
            done();
        })
    })

    describe('Bicicleta.createInstance',()=>{
        it('crea una instancia de biciccleta',()=>{
            var bici= Bicicleta.createInstance('1','verde', 'urbana', [-34.5 , -54,1]);

            expect(bici.code).toBe(1),
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toBe('-34.5');
            expect(bici.ubicacion[1]).toBe('-54.1');

        })
    })

    describe('Bicicletas.allBicis',()=>{
        it('comienza vacia',(done)=>{
            Bicicleta.allBicis((err, bicis)=>{
                expect(Bicis.length).toBe(0)
                (done);
            })

        })
    })

})


// // antes de cada test inicializar allBicis en 0 
// beforeEach(()=>{Bicicleta.allBicis=[]})

// describe('Bicicleta',()=>{
//     it('comienza vacia',()=>{
//         expect(Bicicleta.allBicis.length).toBe(0)
//     })
// })
// describe('Bicicleta.add',()=>{
//     it('agregamos una Bicicleta',()=>{
//         expect(Bicicleta.allBicis.length).toBe(0)

//          var a = new Bicicleta(1, "rojo", "urbana", [-34.601244,-58.3861497])
//          Bicicleta.add(a)
//         expect(Bicicleta.allBicis.length).toBe(1)

//     })
// })

// describe('Bicicleta.findById',()=>{
//     it('deve devolver la bici con id 1',()=>{
//         expect(Bicicleta.allBicis.length).toBe(0)
//         var aBici = new Bicicleta(1,'verde','urbana');
//         var aBici2 = new Bicicleta(2,'roja','urbana');
//         Bicicleta.add(aBici)
//         Bicicleta.add(aBici2)
        
//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(aBici.id);

//     })
// })